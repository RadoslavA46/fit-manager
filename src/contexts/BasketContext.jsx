import { createContext, useState } from "react";

export const BasketContext = createContext();

export function BasketContextProvider({ children }) {
  const [basket, updateBasket] = useState([]);

  const value = {
    basket,
    updateBasket,
  };

  return (
    <BasketContext.Provider value={value}>{children}</BasketContext.Provider>
  );
}
