import { useEffect, useState } from "react";
import { auth, db } from "../config/firebase-config";
import { AuthContext } from "../services/auth.services";
import { onValue, ref } from "firebase/database";
import { getUserByUid } from "../services/users.services";

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(async (user) => {
      if (user) {
        setCurrentUser(user);
        const fetchedData = await getUserByUid(user.uid);

        setCurrentUser(fetchedData);
      } else {
        setCurrentUser(null);
      }
      setLoading(false);
    });

    return () => unsubscribe();
  }, []);

  useEffect(() => {
    const profilePictureRef = ref(db, `users/${currentUser?.uid}/photoURL`);
    const unsubscribe = onValue(profilePictureRef, (snapshot) => {
      if (snapshot.exists()) {
        setCurrentUser((prev) => {
          return { ...prev, photoURL: snapshot.val() };
        });
      }
    });

    return () => unsubscribe();
  }, [currentUser?.uid]);

  const value = {
    currentUser,
  };

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
}
