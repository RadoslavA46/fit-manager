import { Grid, GridItem } from "@chakra-ui/react";
import Desktop from "../Desktop/Desktop.jsx";
import StickyHeader from "../StickyHeader.jsx";
import StickySidebar from "../StickySidebar.jsx";
import "./MainPage.css";
import SidebarButton from "../SidebarButton.jsx";
import { useContext } from "react";
import { useAuth } from "../../../services/auth.services.js";
import { getDateAndTime } from "../../../services/date.and.time.services.js";
import { keyGenerator } from "../../../common/key.generator.js";
import { ONE_DAY } from "../../../consts/prices.js";
import { BasketContext } from "../../../contexts/BasketContext.jsx";
import { useNavigate } from "react-router-dom";

const MainPage = () => {
  const { currentUser } = useAuth();
  const { basket, updateBasket } = useContext(BasketContext);

  const navigate = useNavigate()

  const handleTicketClick = () => {
    updateBasket([
      ...basket,
      {
        type: "ticket",
        member: null,
        price: ONE_DAY,
        date: `${getDateAndTime()[0]}, ${getDateAndTime()[1]}`,
        id: keyGenerator(),
      }
    ]);
  };

  const handleMembershipClick = () => {
    updateBasket([
      ...basket,
      {
        type: "membership",
        member: null,
        price: null,
        date: `${getDateAndTime()[0]}, ${getDateAndTime()[1]}`,
        from: null,
        to: null,
        id: keyGenerator(),
      },
    ]);
  };

  return (
    <div className="main-page">
      <StickyHeader />
      <div className="content-wrapper">
        <StickySidebar>
          <Grid templateColumns="repeat(1, 1fr)" gap={50} >
            <GridItem>
              <SidebarButton
                label="Products"
                action={() => {
                  navigate('/products');
                }}
              />
            </GridItem>
            <GridItem>
              <SidebarButton label={"Day Pass"} action={handleTicketClick} />
            </GridItem>
            <GridItem>
              <SidebarButton
                label={"Membership"}
                action={handleMembershipClick}
              />
            </GridItem>
            <GridItem>
              <SidebarButton
                label={"Log"}
                action={() => {
                  navigate('/log')
                }}
              />
            </GridItem>
            {currentUser?.admin && (
              <GridItem>
                <SidebarButton
                  label={"Admin Panel"}
                  action={() => {
                    navigate('/admin-panel');
                  }}
                />
              </GridItem>
            )}
          </Grid>
        </StickySidebar>
        <Desktop />
      </div>
    </div>
  );
};

export default MainPage;
