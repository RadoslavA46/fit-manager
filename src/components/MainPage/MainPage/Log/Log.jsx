import { useEffect, useState } from "react";
import {
  Box,
  Grid,
  GridItem,
  Text,
} from "@chakra-ui/react";
import { onValue, ref, remove } from "firebase/database";
import { db } from "../../../../config/firebase-config";
import Divider from "../../AdminPanel/Divider";
import DeletePopover from "../../AdminPanel/DeletePopover";

const LogEntry = ({ entry, index, onRemove }) => {
  return (
    <GridItem
      key={entry.id}
      color={index % 2 === 0 ? "black" : "white"}
      backgroundColor={index % 2 === 0 ? "white" : "grey"}
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      padding='5px'
    >
      <Text>
        {!entry.from &&
          !entry.info &&
          `${entry.date} | Day Pass  ${entry.member} ${entry.price}$`}
        {entry.from &&
          `${entry.date} | Membership from: ${entry.from} to: ${entry.to}, ${entry.member} ${entry.price}$`}
        {entry.info && `${entry.date} | Products ${entry.info}`}
      </Text>
      <DeletePopover
        iconColor={index % 2 === 0 ? "red" : "white"}
        header="Delete this log?"
        confirmationQuestion="Are you sure?"
        onPrimaryActionClick={() => {
          onRemove(entry.id)
        }}
      />
    </GridItem>
  );
};

const LogCategory = ({ title, entries, onRemove }) => {
  return (
    <div>
      {entries.length > 0 && (
        <>
          <Text
            marginTop='20px'
            color="white"
            fontWeight='bold'
            fontSize='xl'>
            {title} Sold           
          </Text>
          <Divider />
          {entries.map((entry, index) => (
            <LogEntry
              entry={entry}
              index={index}
              onRemove={onRemove}
              key={entry.id}
            />
          ))}
          <Text
            fontSize='lg'
            color='lightgreen'>
            {title} Total -{" "}
            {entries.reduce((total, entry) => total + entry.price, 0)}$
          </Text>
          <Divider />
        </>
      )}
    </div>
  );
};

const Log = () => {
  const [tickets, setTickets] = useState([]);
  const [memberships, setMemberships] = useState([]);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  const sortFromLatest = (data) => {
    return data.sort((a, b) => {
      const dateA = a.date.split(", ");
      const dateB = b.date.split(", ");

      const [dayA, monthA, yearA] = dateA[0].split("-");
      const [dayB, monthB, yearB] = dateB[0].split("-");
      const [hourA, minuteA, secondA] = dateA[1].split(":");
      const [hourB, minuteB, secondB] = dateB[1].split(":");

      const dateObjA = new Date(
        yearA,
        monthA - 1,
        dayA,
        hourA,
        minuteA,
        secondA,
      );
      const dateObjB = new Date(
        yearB,
        monthB - 1,
        dayB,
        hourB,
        minuteB,
        secondB,
      );

      return dateObjB - dateObjA;
    });
  };

  useEffect(() => {
    const fetchData = (path, stateUpdater) => {
      const dataRef = ref(db, path);
      return onValue(dataRef, (snapshot) => {
        const data = [];
        if (snapshot.exists()) {
          snapshot.forEach((childSnapshot) => {
            data.push(childSnapshot.val());
          });
        }

        const sortedData = sortFromLatest(data);
        stateUpdater(sortedData);
      });
    };

    const unsubscribeTickets = fetchData("log/tickets", setTickets);
    const unsubscribeMemberships = fetchData("log/memberships", setMemberships);
    const unsubscribeProducts = fetchData("log/products", setProducts);

    return () => {
      unsubscribeTickets();
      unsubscribeMemberships();
      unsubscribeProducts();
      setLoading(false);
    };
  }, []);

  const handleRemoveEntry = (path, entryId) => {
    remove(ref(db, `${path}/${entryId}`));
  };

  return (
    !loading && (
      <Box backgroundColor="#212121" overflowY="auto" padding="20px" flex="1">
        <Grid width="800px">
          <LogCategory
            title="Tickets"
            entries={tickets}
            onRemove={(entryId) => handleRemoveEntry("log/tickets", entryId)}
          />
          <LogCategory
            title="Memberships"
            entries={memberships}
            onRemove={(entryId) =>
              handleRemoveEntry("log/memberships", entryId)
            }
          />
          <LogCategory
            title="Products"
            entries={products}
            onRemove={(entryId) => handleRemoveEntry("log/products", entryId)}
          />

          <Text
            fontSize='xl'
            margin='10px'
            color='lightgreen'>
            Total sales -{" "}
            {tickets.reduce((total, entry) => total + entry.price, 0) +
              memberships.reduce((total, entry) => total + entry.price, 0) +
              products.reduce((total, entry) => total + entry.price, 0)}$
          </Text>
        </Grid>
      </Box>
    )
  );
};

export default Log;
