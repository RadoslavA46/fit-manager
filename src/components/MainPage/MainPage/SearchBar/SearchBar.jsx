import { get, ref } from "firebase/database";
import { useEffect, useState } from "react";
import { db } from "../../../../config/firebase-config";
import {
  Input,
  InputGroup,
  InputRightElement,
  List,
  ListItem,
} from "@chakra-ui/react";
import { SearchIcon } from "@chakra-ui/icons";

const SearchBar = ({ handleMemberSelect }) => {
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const handleInputChange = (event) => {
    setSearchQuery(event.target.value);
  };

  useEffect(() => {
    if (searchQuery !== "") {
      const databaseRef = ref(db, "/members");

      get(databaseRef)
        .then((snapshot) => {
          const data = snapshot.val();
          const filteredResults = Object.entries(data).filter(([key, value]) =>
            key.includes(searchQuery),
          );
          setSearchResults(filteredResults);
        })
        .catch((error) => {
          alert(error.message);
        });
    } else {
      setSearchResults([]); // Clear search results when searchQuery is empty
    }
  }, [searchQuery]);

  return (
    <div>
      <InputGroup>
        <Input
          type="text"
          placeholder="Enter CPR nr..."
          value={searchQuery}
          onChange={handleInputChange}
          color="black"
        />
        <InputRightElement>
          <SearchIcon color="gray.400" />
        </InputRightElement>
      </InputGroup>
      <List mt={2}>
        {searchResults.map((member, index) => (
          <ListItem
            color={"black"}
            key={index}
            p={2}
            borderBottom="1px solid #E2E8F0"
            cursor="pointer"
            _hover={{ backgroundColor: "gray.400" }}
            onClick={() => {
              handleMemberSelect(
                `${member[1].firstName} ${member[1].lastName}`,
              );
            }}
          >
            {`${member[1].firstName} ${member[1].lastName}, CPRnr - ${
              member[0]
            } ${member[1].isBanned ? "BANNED" : ""}`}
          </ListItem>
        ))}
      </List>
    </div>
  );
};

export default SearchBar;
