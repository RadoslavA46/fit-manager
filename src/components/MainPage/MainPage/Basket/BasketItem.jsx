import { Text } from "@chakra-ui/react";

const BasketItem = ({ order }) => {
  const total = order.count * order.price;

  return (

    <Text as="div" fontSize="14px" color="white" display="flex" marginTop='10px'>
      <Text fontWeight="bold">{order.count} x</Text>
      {order.price} {order.productName}
      <Text marginLeft="4px" fontWeight="bold">
        {total}$
      </Text>
    </Text>
  );
};

export default BasketItem;
