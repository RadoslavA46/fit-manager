import { Button } from "@chakra-ui/react";

const SidebarButton = ({ label, action }) => {
  return ( 
    <Button
      onClick={() => {
        action();
      }}
      width="200px"
      height="80px"
      fontSize="22px"
      color={"#212121"}
      bg="#355C7D"
      fontWeight="bold"
      _hover={{
        transform: "translateY(-2px)",
        boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
      }}
    >
      {label}
    </Button>
  );
};

export default SidebarButton;
