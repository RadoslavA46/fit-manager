import { useState } from "react";
import {
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  FormLabel,
  Input,
  Select,
  Stack,
  useDisclosure,
} from "@chakra-ui/react";
import { registerMember } from "../../services/members.services";
import { CPR_LENGTH, MAX_ADDRESS_LENGTH, MIN_ADDRESS_LENGTH, NAME_MAX_LENGTH, NAME_MIN_LENGTH } from "../../common/consts";

export default function NewMemberForm({ handleMemberSelect }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [sex, setSex] = useState("man"); 
  const [address, setAddress] = useState("");
  const [cprNr, setCprNr] = useState("");

  const resetFields = () => {
    setFirstName("");
    setLastName("");
    setAddress("");
    setCprNr("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (firstName.length < NAME_MIN_LENGTH || firstName.length > NAME_MAX_LENGTH) {
        alert(`The first name must be between ${NAME_MIN_LENGTH} and ${NAME_MAX_LENGTH} symbols.`);
      } else if (lastName.length < NAME_MIN_LENGTH || lastName.length > NAME_MAX_LENGTH) {
        alert(`The last name must be between ${NAME_MIN_LENGTH} and ${NAME_MAX_LENGTH} symbols.`);
      } else if (address.length < MIN_ADDRESS_LENGTH || address.length > MAX_ADDRESS_LENGTH) {
        alert(`The address must be between ${MIN_ADDRESS_LENGTH} and ${MAX_ADDRESS_LENGTH} symbols.`);
      } else if (cprNr.length !== CPR_LENGTH || isNaN(cprNr)) {
        alert(`The CPRnr must be a ${CPR_LENGTH}-digit number.`);
      } else {
        await registerMember(firstName, lastName, sex, address, cprNr);
        handleMemberSelect(`${firstName} ${lastName}`);
        resetFields();
        onClose();
      }
    } catch (error) {
      resetFields();
      console.log(error.message);
    }
  };

  return (
    <>
      <Button size={"sm"} colorScheme="teal" onClick={onOpen}>
        New
      </Button>
      <Drawer isOpen={isOpen} placement="left" onClose={onClose}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton onClick={resetFields}/>
          <DrawerHeader borderBottomWidth="1px">
            Register a new member
          </DrawerHeader>

          <form onSubmit={handleSubmit}>
            <DrawerBody>
              <Stack spacing="24px">
                <Box>
                  <FormLabel htmlFor="firstName">First Name</FormLabel>
                  <Input
                    type="text"
                    id="firstName"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    placeholder="First name"
                  />
                </Box>
                <Box>
                  <FormLabel htmlFor="lastName">Last Name</FormLabel>
                  <Input
                    type="text"
                    id="lastName"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    placeholder="Last name"
                  />
                </Box>
                <Box>
                  <FormLabel htmlFor="sex">Sex</FormLabel>
                  <Select
                    id="sex"
                    value={sex}
                    onChange={(e) => setSex(e.target.value)}
                  >
                    <option value="man">Man</option>
                    <option value="woman">Woman</option>
                    <option value="other">Other</option>
                  </Select>
                </Box>
                <Box>
                  <FormLabel htmlFor="address">Address</FormLabel>
                  <Input
                    type="text"
                    id="address"
                    value={address}
                    onChange={(e) => setAddress(e.target.value)}
                    placeholder="Please enter an address"
                  />
                </Box>
                <Box>
                  <FormLabel htmlFor="cprNr">CPR Nr</FormLabel>
                  <Input
                    type="text"
                    id="cprNr"
                    value={cprNr}
                    onChange={(e) => setCprNr(e.target.value)}
                    placeholder="Please enter a CPR nr"
                  />
                </Box>
              </Stack>
            </DrawerBody>

            <DrawerFooter borderTopWidth="1px">
              <Button
                type="button"
                variant="outline"
                mr={3}
                onClick={() => {
                  resetFields();
                  onClose();
                }}
              >
                Cancel
              </Button>
              <Button type="submit" colorScheme="blue">
                Register
              </Button>
            </DrawerFooter>
          </form>
        </DrawerContent>
      </Drawer>
    </>
  );
}
