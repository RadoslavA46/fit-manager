import { useContext, useEffect, useState } from "react";
import { logMembership, logTicket } from "../../../../services/log.services";
import { Button } from "@chakra-ui/react";
import { BasketContext } from "../../../../contexts/BasketContext";
import { registerPaidProducts } from "../../../../services/products.services";
import DeletePopover from "../../AdminPanel/DeletePopover.jsx";


const PayAllButton = () => {
  const [bill, setBill] = useState(0);
  const { basket, updateBasket } = useContext(BasketContext);

  useEffect(() => {
    let sum = 0;
    basket.forEach((item) => {
      if (item.type === 'product') {
        sum += +item.price * +item.count;
      }
      if (item.type === 'ticket' && item.member) {
        sum += +item.price
      }
      if (item.type === 'membership' && item.member && item.from && item.to) {
        sum += +item.price
      }
    });
    setBill(sum);
  }, [basket]);

  const payAndRegister = () => {
    try {
      const arrayOfIdsforDelete = basket.reduce((acc, item) => {
        console.log(item)
        if (
          item.type === "membership" &&
          item.member &&
          item.price &&
          item.from
        ) {
          try {
            logMembership(
              item.member,
              item.date,
              item.price,
              item.id,
              item.from,
              item.to,
            );
            console.log(item.id, "registered");
          } catch (membershipError) {
            console.error("Error in logMembership:", membershipError);
          }
          acc.push(item.id);
        }

        if (item.type === "ticket" && item.member) {
          try {
            logTicket(item.member, item.date, item.price, item.id);
            console.log(item.id, "registered");
          } catch (ticketError) {
            console.error("Error in logTicket:", ticketError);
          }
          acc.push(item.id);
        }
        return acc;
      }, []);

      updateBasket((basket) => basket.filter((basketItem) => !arrayOfIdsforDelete.includes(basketItem.id)))
      registerPaidProducts(basket);
      updateBasket([]);
    } catch (error) {
      console.error("Error in payAndRegister:", error);
    }
  };

  return (
    bill > 0 && (
      <DeletePopover
        triggerElement={
          <Button colorScheme="teal" size={"lg"}>
            Pay All - {bill}$
          </Button>
        }
        header="Payment"
        confirmationQuestion="Did you receive the money?"
        onPrimaryActionClick={payAndRegister}
      />
    )
  );
};

export default PayAllButton;
