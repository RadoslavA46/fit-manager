import { Box, Grid, GridItem } from "@chakra-ui/react";
import OneDayPass from "../../OneDayPass/OneDayPass";
import Membership from "../../Membership/Membership";
import { DeleteIcon } from "@chakra-ui/icons";
import PayAllButton from "./PayAllButton/PayAllButton";
import { useContext, useEffect, useState } from "react";
import { BasketContext } from "../../../contexts/BasketContext";
import ProductsBasketCard from "./ProductsBasketCard/ProductsBasketCard";

const Desktop = () => {
  const { basket, updateBasket } = useContext(BasketContext);
  const [itemsReadyForPayment, setItemsReadyForPayment] = useState(false)

  useEffect(() => {
    let count = 0;
    basket.forEach((basketItem) => {
      if (basketItem.type === 'product') {
        count++;
      }
      if (basketItem.type === 'ticket' && basketItem.member) {
        count++;
      }
      if (basketItem.type === 'membership' && basketItem.member && basketItem.from && basketItem.to) {
        count++;
      }
    });

    if (count > 0) {
      setItemsReadyForPayment(true);
    }
  }, [basket]);

  const productsSelected = () => {
    let answer = false
    basket.forEach((item) => {
      if (item.type === 'product') {
        answer = true
      }
    })
    return answer
  };

  
  return (
    <div className="main-content">
      {itemsReadyForPayment && (
        <Box
          margin={"15px"}
          alignContent="center"
          display="flex"
          justifyContent="center"
        >
          <PayAllButton />
        </Box>
      )}

      <Grid templateColumns="repeat(4, 1fr)" gap={6}>
        {productsSelected() && 
          <>
            {" "}
            <GridItem >
              <ProductsBasketCard />
            </GridItem>
          </>
        }
        {basket.map((item) => {
          if (item.type === "ticket") {
            return (
              <div key={item.id}>
                <GridItem >
                  <OneDayPass info={item} />
                  <Box w="50%">
                    <p style={{ textAlign: "center" }}>
                      <DeleteIcon
                        cursor="pointer"
                        onClick={() => {
                          updateBasket((basket) => {
                            return basket.filter((basketItem) => basketItem.id !== item.id)
                          })
                        }}
                        color={"tomato"}
                      />
                    </p>
                  </Box>
                </GridItem>
              </div>
            );
          } else if (item.type === "membership") {
            return (
              <div key={item.id}>
                <GridItem>
                  <Membership info={item} />
                  <Box w="50%">
                    <p style={{ textAlign: "center" }}>
                      <DeleteIcon
                        cursor="pointer"
                        onClick={() => {
                          updateBasket((basket) => {
                            return basket.filter((basketItem) => basketItem.id !== item.id)
                          })
                        }}
                        color={"tomato"}
                      />
                    </p>
                  </Box>
                </GridItem>
              </div>
            );
          }
        })}
      </Grid>
    </div>
  );
};

export default Desktop;
