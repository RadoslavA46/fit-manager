import {
  Box,
  Button,
  ButtonGroup,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Portal,
  Text,
} from "@chakra-ui/react";
import { registerPaidProducts } from "../../../../services/products.services";
import { useContext } from "react";
import { BasketContext } from "../../../../contexts/BasketContext";
import { useNavigate } from "react-router-dom";

const ProductBasketCard = () => {

  const { basket, updateBasket } = useContext(BasketContext);
  const navigate = useNavigate()

  // Calculate the total price
  const calculateTotalPrice = () => {
    let totalPrice = 0;
    basket.forEach((item) => {
      if (item.type === 'product') {
        totalPrice += item.count * item.price;
      }
    });
    return totalPrice;
  };

  const totalPrice = calculateTotalPrice();

  return (
    <Box
      width="300px"
      height="350px"
      background="#dea881"
      borderWidth="1px"
      borderRadius="md"
      boxShadow="md"
      p="4"
      display="flex"
      flexDirection="column"
    >
      {/* Header */}
      <Text fontSize="2xl" fontWeight="bold">
        Products Basket
      </Text>

      {/* Body */}
      <Box flex="1">
        {basket.length > 0 ? (
          basket.map((item) => (

            item.type === 'product' && <Text key={item.id}>
              {`${item.count} x ${item.price}$ ${item.productName} ${item.count * item.price
                }$`}
            </Text>
          ))
        ) : (
          <Text>Your basket is empty.</Text>
        )}
        --------------
        <br />
        Total: {totalPrice}$
      </Box>

      {/* Footer */}
      <ButtonGroup>
        <Button colorScheme="gray" onClick={() => { navigate('/products') }}>
          Edit List
        </Button>
        <Popover closeOnBlur={false} placement="right">
          {({ onClose }) => (
            <>
              <PopoverTrigger>
                <Button colorScheme="teal">Pay {totalPrice}$</Button>
              </PopoverTrigger>
              <Portal>
                <PopoverContent>
                  <PopoverHeader>Payment</PopoverHeader>
                  <PopoverCloseButton />
                  <PopoverBody>
                    <Box>Did you receive the money?</Box>
                    <ButtonGroup size="sm">
                      <Button
                        variant="outline"
                        color="white"
                        bg="green"
                        onClick={() => {
                          registerPaidProducts(basket);
                          updateBasket((current) => current.filter(item=> item.type !== 'product'));
                        }}
                      >
                        Yes
                      </Button>
                      <Button onClick={onClose} colorScheme="red">
                        No
                      </Button>
                    </ButtonGroup>
                  </PopoverBody>
                </PopoverContent>
              </Portal>
            </>
          )}
        </Popover>
      </ButtonGroup>
    </Box>
  );
};

export default ProductBasketCard;
