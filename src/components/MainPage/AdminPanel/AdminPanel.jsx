import { onValue, ref } from "firebase/database";
import { useEffect, useState } from "react";

import { db } from "../../../config/firebase-config";
import { Box, Grid, Text, GridItem, Button } from "@chakra-ui/react";
import {
  toggleBanMember,
  deleteMember,
} from "../../../services/members.services";
import { toggleAdminUser } from "../../../services/users.services";
import { useAuth } from "../../../services/auth.services";
import SectionHeader from "./SectionHeader.jsx";
import DeletePopover from "./DeletePopover.jsx";

const styles = {
  button: {
    height: "21px",
    padding: "7px",
    textTransform: "uppercase",
    fontWeight: 400,
    fontSize: '10px',
    width: '50px'
  },
  row: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '5px'
  }
};

const AdminPanel = () => {
  const [users, setUsers] = useState([]);
  const [members, setMembers] = useState([]);
  const [loading, setLoading] = useState(true);

  const { currentUser } = useAuth();

  const getRowStyle = (index) => {
    return {
      ...styles.row,
      backgroundColor: index % 2 === 0 ? "white" : "grey",
      color: index % 2 === 0 ? "black" : "white"
    }
  }

  useEffect(() => {
    const fetchData = (path, stateUpdater) => {
      const dataRef = ref(db, path);
      const unsubscribe = onValue(dataRef, (snapshot) => {
        const data = [];
        if (snapshot.exists()) {
          snapshot.forEach((childSnapshot) => {
            data.push(childSnapshot.val());
          });
        }
        stateUpdater(data);
      });

      return unsubscribe;
    };

    const unsubscribeUsers = fetchData("/users", setUsers);
    const unsubscribeMembers = fetchData("/members", setMembers);

    return () => {
      unsubscribeUsers();
      unsubscribeMembers();
      setLoading(false);
    };
  }, []);

  const getGenderLabel = (gender) => {
    switch (gender) {
      case 'man': {
        return '(M)'
      }
      case 'woman': {
        return '(F)'
      }
      case 'other': {
        return '(O)'
      }
      default: {
        return ''
      }
    }
  }

  return (
    <>
      {!loading && (
        <Box backgroundColor="#212121" overflowY="auto" padding="20px" flex="1">
          <Grid maxWidth="800px">
            {users.length > 0 && (
              <>
                <SectionHeader title="Manage Users" />
                {users.length > 0 &&
                  users
                    .filter((user) => user.email !== currentUser.email)
                    .map((user, index) => {
                      const userIsAdmin = user?.admin;
                      return (
                        <GridItem
                          key={user.email}
                          {...getRowStyle(index)}
                        >
                          <Text>{`${user.email}`}</Text>
                          <Button
                            {...styles.button}
                            onClick={() => toggleAdminUser(user.uid)}
                            colorScheme={userIsAdmin ? "green" : "red"}
                          >
                            {userIsAdmin ? "Admin" : "User"}
                          </Button>
                        </GridItem>
                      );
                    })}
              </>
            )}

            {members.length > 0 && (
              <>
                <SectionHeader title="Manage Members" />
                {members.map((member, index) => {
                  const memberIsAllowed = !member?.isBanned;
                  return (
                    <GridItem
                      key={member.cprNr}
                      {...getRowStyle(index)}
                    >
                      <Text fontSize='14px'>
                        {member.firstName} {member.lastName} {getGenderLabel(member.sex)} -
                        CPR {member.cprNr}
                      </Text>
                      <div>
                        <Button
                          {...styles.button}
                          marginRight='5px'
                          onClick={() => toggleBanMember(member.cprNr)}
                          colorScheme={memberIsAllowed ? "green" : "red"}
                        >
                          {memberIsAllowed ? "Active" : "Banned"}
                        </Button>
                        <DeletePopover
                          iconColor={index % 2 === 0 ? "red" : "white"}
                          header="Delete this member?"
                          confirmationQuestion="Are you sure"
                          onPrimaryActionClick={() => {
                            deleteMember(member.cprNr);
                          }}
                        />
                      </div>
                    </GridItem>
                  );
                })}
              </>
            )}
          </Grid>
        </Box>
      )}
    </>
  );
};

export default AdminPanel;
