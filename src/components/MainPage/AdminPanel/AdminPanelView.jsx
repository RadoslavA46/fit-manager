import { Grid, GridItem } from "@chakra-ui/react";
import SidebarButton from "../SidebarButton";
import StickySidebar from "../StickySidebar";
import StickyHeader from "../StickyHeader";
import { useNavigate } from "react-router-dom";
import AdminPanel from "./AdminPanel";

const AdminPanelView = () => {
  const navigate = useNavigate()
  return (
    <div className="main-page">
      <StickyHeader />
      <div className="content-wrapper">
        <StickySidebar>
          <Grid templateColumns="repeat(1, 1fr)" gap={50}>
            <GridItem>
              <SidebarButton label='Back' action={() => navigate('/')} />
            </GridItem>
          </Grid>
        </StickySidebar>
        <AdminPanel />

      </div>
    </div>
  );
};

export default AdminPanelView;