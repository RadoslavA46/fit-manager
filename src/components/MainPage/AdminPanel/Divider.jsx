import { Divider as ChakraDivider } from "@chakra-ui/react";

const Divider = ({
  style = {
    height: "3px",
    marginBottom: "3px",
  },
}) => {
  return (
    <div style={style}>
      <ChakraDivider />
    </div>
  );
};

export default Divider;
