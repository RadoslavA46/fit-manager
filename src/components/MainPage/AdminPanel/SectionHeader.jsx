import { Text } from "@chakra-ui/react";
import Divider from "./Divider.jsx";

const SectionHeader = ({ title, dividerStyle, showDivider = true }) => {
  return (
    <div style={{ margin: "5px 0" }}>
      <Text color="white">{title}</Text>
      {showDivider && <Divider style={dividerStyle} />}
    </div>
  );
};

export default SectionHeader;
