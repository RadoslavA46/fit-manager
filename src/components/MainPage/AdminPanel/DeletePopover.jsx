import {
  Box,
  Button,
  ButtonGroup,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Portal,
} from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";

const DeletePopover = (props) => {
  const {
    confirmationQuestion,
    header,
    onPrimaryActionClick,
    onCancelClick,
    iconColor = "red",
    placement = "right",
    primaryActionLabel = "Yes",
    cancelActionLabel = "No",
    closeOnBlur = false,
    triggerElement = <DeleteIcon cursor="pointer" color={iconColor} />,
  } = props;
  return (
    <Popover closeOnBlur={closeOnBlur} placement={placement}>
      {({ isOpen, onClose }) => (
        <>
          <PopoverTrigger>
            {triggerElement}
          </PopoverTrigger>
          <Portal>
            <PopoverContent>
              <PopoverHeader>{header}</PopoverHeader>
              <PopoverCloseButton />
              <PopoverBody>
                <Box>{confirmationQuestion}</Box>
                <ButtonGroup size="sm">
                  <Button
                    variant="outline"
                    color="white"
                    bg="green"
                    onClick={() => {
                      onPrimaryActionClick?.();
                      onClose();
                    }}
                  >
                    {primaryActionLabel}
                  </Button>
                  <Button
                    onClick={() => {
                      onCancelClick?.();
                      onClose();
                    }}
                    colorScheme="red"
                  >
                    {cancelActionLabel}
                  </Button>
                </ButtonGroup>
              </PopoverBody>
            </PopoverContent>
          </Portal>
        </>
      )}
    </Popover>
  );
};

export default DeletePopover;
