import { Grid, GridItem } from "@chakra-ui/react";
import SidebarButton from "../SidebarButton";
import StickySidebar from "../StickySidebar";
import StickyHeader from "../StickyHeader";
import Log from "../MainPage/Log/Log";
import { useNavigate } from "react-router-dom";

const LogView = () => {
  const navigate = useNavigate()
  return (
    <div className="main-page">
      <StickyHeader />
      <div className="content-wrapper">
        <StickySidebar>
          <Grid templateColumns="repeat(1, 1fr)" gap={50}>
            <GridItem>
              <SidebarButton label='Back' action={() => navigate('/')} />
            </GridItem>
          </Grid>
        </StickySidebar>
        <Log />

      </div>
    </div>
  );
};

export default LogView;