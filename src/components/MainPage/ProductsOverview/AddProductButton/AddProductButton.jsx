import { AddIcon } from "@chakra-ui/icons";
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import { useState } from "react";
import { addNewProduct } from "../../../../services/products.services";
import { keyGenerator } from "../../../../common/key.generator";
import { productPicturesRef } from "../../../../config/firebase-config";
import { getDownloadURL, uploadBytes } from "firebase/storage";
import { ref as sRef } from "firebase/storage";

const AddProductButton = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const [productName, setProductName] = useState("");
  const [productPrice, setProductPrice] = useState("");
  const [image, setImage] = useState("");

  const handleProductName = (e) => {
    setProductName(e.target.value);
  };

  const handleProductPrice = (e) => {
    setProductPrice(e.target.value);
  };

  const handleImage = (e) => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
  };

  const getImageURL = async () => {
    if (image) {
      try {
        const uniqueImageName = keyGenerator();
        const imageRef = sRef(productPicturesRef, uniqueImageName);
        await uploadBytes(imageRef, image);
        const url = await getDownloadURL(imageRef);

        return url;
      } catch (error) {
        console.log(error);
      }
    }
    return null;
  };

  const handleAddProduct = async () => {
    // VALIDATIONS!!!

    try {
      if (productName.length < 2 || productName.length > 20) {
        alert("The Product name must be between 2 and 20 symbols.");
      } else if (
        isNaN(productPrice) ||
        productPrice < 0 ||
        productPrice > 10000
      ) {
        alert("The Price must be between 0 and 10 000.");
      } else {
        const url = await getImageURL();

        addNewProduct(productName, productPrice, url, keyGenerator());
        onClose();
        setProductName("");
        setProductPrice("");
        setImage("");
      }
    } catch (error) {
      console.log(`Error in Add Product: ${error}`);
    }
  };

  return (
    <>
      <Button onClick={onOpen} colorScheme="teal" color={"black"}>
        <AddIcon />
        Add New Product
      </Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add product</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl isRequired>
              <FormLabel>Product Name</FormLabel>
              <Input
                onChange={handleProductName}
                placeholder="Enter the name of the product"
              />
            </FormControl>

            <FormControl isRequired mt={4}>
              <FormLabel>Product Price</FormLabel>
              <Input
                onChange={handleProductPrice}
                type="number"
                placeholder="Enter the price of the product"
              />
            </FormControl>

            <FormControl>
              <FormLabel>Product Photo</FormLabel>
              <Input
                onChange={handleImage}
                type="file"
                placeholder="Upload a picture"
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button onClick={handleAddProduct} colorScheme="blue" mr={3}>
              Add
            </Button>
            <Button
              onClick={() => {
                onClose();
                setProductName("");
                setProductPrice("");
                setImage("");
              }}
            >
              Cancel
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AddProductButton;
