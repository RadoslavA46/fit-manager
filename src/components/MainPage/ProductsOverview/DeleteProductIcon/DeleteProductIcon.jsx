import { DeleteIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  ButtonGroup,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Portal,
} from "@chakra-ui/react";
import { deleteProduct } from "../../../../services/products.services";

const DeleteProductIcon = ({ productID }) => {
  return (
    <Popover closeOnBlur={false} placement="right">
      {({ isOpen, onClose }) => (
        <>
          <PopoverTrigger>
            <DeleteIcon color="tomato" cursor="pointer" />
          </PopoverTrigger>
          <Portal>
            <PopoverContent>
              <PopoverHeader>Delete this product</PopoverHeader>
              <PopoverCloseButton />
              <PopoverBody>
                <Box>Are you sure?</Box>
                <ButtonGroup size="sm">
                  <Button
                    variant="outline"
                    color="white"
                    bg="green"
                    onClick={() => {
                      onClose();
                      deleteProduct(productID);
                    }}
                  >
                    Yes
                  </Button>
                  <Button onClick={onClose} colorScheme="red">
                    No
                  </Button>
                </ButtonGroup>
              </PopoverBody>
            </PopoverContent>
          </Portal>
        </>
      )}
    </Popover>
  );
};

export default DeleteProductIcon;
