import { Box, Grid, GridItem, Text } from "@chakra-ui/react";
import ProductCard from "./ProductCard/ProductCard";
import "./ProductsView.css";
import AddProductButton from "./AddProductButton/AddProductButton";
import { useContext, useEffect, useState } from "react";
import { onValue, ref } from "firebase/database";
import { db } from "../../../config/firebase-config";
import DeleteProductIcon from "./DeleteProductIcon/DeleteProductIcon";
import { useAuth } from "../../../services/auth.services";
import StickyHeader from "../StickyHeader";
import StickySidebar from "../StickySidebar";
import SidebarButton from "../SidebarButton";
import BasketItem from "../MainPage/Basket/BasketItem";
import { BasketContext } from "../../../contexts/BasketContext";
import { useNavigate } from "react-router-dom";

const ProductsView = () => {
  const [products, setProducts] = useState("");
  const [loading, setLoading] = useState(true);
  const { currentUser } = useAuth();
  const { basket } = useContext(BasketContext)
  const [productsReadyForPayment, setProductsReadyForPayment] = useState(false)
  const navigate = useNavigate()

  useEffect(() => {
    const productsRef = ref(db, "/products");
    const unsubscribe = onValue(productsRef, (snapshot) => {
      const data = [];
      if (snapshot.exists()) {
        snapshot.forEach((childSnapshot) => {
          const productData = childSnapshot.val();
          data.push(productData);
        });
      }
      setLoading(false);
      setProducts(data);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    const productsSelected = () => {
      let count = 0
      basket.forEach((item) => {
        if (item.type === 'product') {
          count++
        }
      })
      if (count > 0) {
        setProductsReadyForPayment(true)
      }
    };
    productsSelected();
  }, [basket]);



  return (
    <div className="main-page">
      <StickyHeader />
      <div className="content-wrapper">
        <StickySidebar>
          <Grid templateColumns="repeat(1, 1fr)" gap={50}>
            <GridItem>
              <SidebarButton label='Back' action={() => navigate('/')} />
            </GridItem>
          </Grid>
          {!productsReadyForPayment ? (
            <Text textAlign="center" fontSize="18px" color="white" marginTop='20px'>
              No products selected
            </Text>
          ) : (
            <>
              <Text marginBottom={"10px"} color="white" fontSize="18px" marginTop='20px'>
                Added to basket:
              </Text>
              {basket.filter((basketItem) => basketItem.type === 'product').map((order) => (
                <BasketItem key={order.id} order={order} />
              ))}
            </>
          )}


        </StickySidebar>
        <div className="products-wrapper">
          <Box
            margin={"15px"}
            alignContent="center"
            display="flex"
            justifyContent="center"
          >
            {currentUser.admin && <AddProductButton />}
          </Box>

          <Grid templateColumns="repeat(4, 1fr)" gap={6}>
            {!loading &&
              products.map((product) => {
                return (
                  <GridItem key={product.id}>
                    <ProductCard product={product} />
                    <Box marginLeft="120px">
                      {currentUser.admin && (
                        <DeleteProductIcon productID={product.id} />
                      )}
                    </Box>
                  </GridItem>
                );
              })}
          </Grid>
        </div>
      </div>
    </div>
  );
};

export default ProductsView;
