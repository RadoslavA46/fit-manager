import {
  Box,
  Flex,
  Text,
  Image,
  IconButton,
  ButtonGroup,
} from "@chakra-ui/react";
import { AddIcon, MinusIcon } from "@chakra-ui/icons";
import { useContext } from "react";
import { BasketContext } from "../../../../contexts/BasketContext";

const ProductCard = ({ product }) => {
  const { basket, updateBasket } = useContext(BasketContext);

  const getAmount = () => {
    const itemInBasket = basket.find((basketItem) => {
      return basketItem.id === product.id;
    });
    return itemInBasket ? itemInBasket.count : 0;
  };

  const _amount = getAmount();

  const image =
    product.imageURL ||
    "https://as2.ftcdn.net/v2/jpg/04/70/29/97/1000_F_470299797_UD0eoVMMSUbHCcNJCdv2t8B2g1GVqYgs.jpg";

  const handleClickMinus = () => {
    updateBasket((basket) => {
      return basket
        .map((basketItem) => {
          if (basketItem.id === product.id) {
            if (basketItem.count === 1) {
              return undefined;
            } else {
              return { ...basketItem, count: basketItem.count - 1 };
            }
          } else {
            return basketItem;
          }
        })
        .filter((basketItem) => basketItem !== undefined);
    });
  };

  const handleClickPlus = () => {
    const productAlreadyInBasket = basket.find(
      (item) => item.id === product.id,
    );
    if (productAlreadyInBasket) {
      updateBasket((basket) => {
        return basket.map((basketItem) => {
          if (basketItem.id=== product.id) {
            return { ...basketItem, count: basketItem.count + 1 };
          } else {
            return basketItem;
          }
        });
      });
    } else {
      updateBasket([...basket, { ...product, count: 1 }]);
    }
  };

  return (
    <Box
      bg="white"
      height="320px"
      width="250px"
      borderWidth="1px"
      borderRadius="md"
      boxShadow="md"
      p="4"
      display="flex"
      flexDirection="column"
    >
      {/* Header */}
      <Flex justifyContent="flex-end">
        <ButtonGroup>
          <IconButton
            icon={<AddIcon />}
            size="sm"
            colorScheme="green"
            aria-label="Add Product"
            width="100px"
            onClick={handleClickPlus}
          />
          <IconButton
            icon={<MinusIcon />}
            size="sm"
            colorScheme="red"
            aria-label="Remove Product"
            width="100px"
            onClick={handleClickMinus}
            isDisabled={_amount <= 0}
          />
        </ButtonGroup>
      </Flex>

      {/* Body */}
      <Flex flexDirection="column" alignItems="center" flex="1">
        {/* Product Name */}
        <Text fontSize="xl" fontWeight="bold" my="2">
          {product.productName}
        </Text>

        {/* Product Image */}
        <Image src={image} alt={product.productName} boxSize="120px" />
        <Text fontSize="lg" fontWeight="bold" my="2">
          {`${product.price}$`}
        </Text>
      </Flex>

      {/* Footer */}
      <Box mt="2">
        <Text fontSize="lg" fontWeight="bold">
          Amount: {_amount}
        </Text>
      </Box>
    </Box>
  );
};

export default ProductCard;
