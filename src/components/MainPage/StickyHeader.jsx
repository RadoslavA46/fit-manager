import { Box, Flex, Spacer, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import UserInfo from "../UserInfo/UserInfo";

const StickyHeader = () => {
  const [currentTime, setCurrentTime] = useState(new Date());

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentTime(new Date());
    }, 1000 * 60);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  // Format the time as "HH:MM AM/PM"
  const formattedTime = currentTime.toLocaleTimeString([], {
    hour: "2-digit",
    minute: "2-digit",
    hour12: true,
  });

  return (
    <div className="header">
      <Flex p="5px" alignItems="center">
        <Box>
          <Text fontSize="50px" color="#212121">
            {formattedTime}
          </Text>
        </Box>
        <Spacer />
        <Text
          fontSize="80px"
          padding="10px"
          fontFamily="fantasy"
          color="#212121"
        >
          Fit Manager
        </Text>
        <Spacer />
        <Box>
          <UserInfo />
        </Box>
      </Flex>
    </div>
  );
};

export default StickyHeader;
