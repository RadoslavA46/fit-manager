import {
  Box,
  Button,
  ButtonGroup,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Portal,
} from "@chakra-ui/react";

const PayButton = ({ payAndRegister, price }) => {
  return (
    <Popover closeOnBlur={false} placement="right">
      {({ isOpen, onClose }) => (
        <>
          <PopoverTrigger>
            <Button colorScheme="teal" size={"md"}>{`Pay-${price}$`}</Button>
          </PopoverTrigger>
          <Portal>
            <PopoverContent>
              <PopoverHeader>Payment</PopoverHeader>
              <PopoverCloseButton />
              <PopoverBody>
                <Box>Did you receive the money?</Box>
                <ButtonGroup size="sm">
                  <Button
                    variant="outline"
                    color="white"
                    bg="green"
                    onClick={() => {
                      payAndRegister();
                    }}
                  >
                    Yes
                  </Button>
                  <Button onClick={onClose} colorScheme="red">
                    No
                  </Button>
                </ButtonGroup>
              </PopoverBody>
            </PopoverContent>
          </Portal>
        </>
      )}
    </Popover>
  );
};

export default PayButton;
