import { useNavigate } from "react-router-dom";
import { logOutUser, useAuth } from "../../services/auth.services";
import { Box, Button, Flex, Text } from "@chakra-ui/react";
import ProfilePicture from "./ProfilePicture/ProfilePicture";

const UserInfo = () => {
  const { currentUser } = useAuth();
  const navigate = useNavigate();

  return (
    <>
      {currentUser && (
        <Flex alignItems="center">
          <ProfilePicture />
          <Box ml={3}>
            <Text fontWeight="bold">{currentUser?.email}</Text>
            {currentUser?.admin && (
              <Text fontSize="sm" color="#dea881">
                Admin
              </Text>
            )}
            <Button
              size="sm"
              onClick={() => {
                logOutUser();
                navigate("/sign-in");
              }}
              mt={1}
            >
              Log out
            </Button>
          </Box>
        </Flex>
      )}
    </>
  );
};

export default UserInfo;
