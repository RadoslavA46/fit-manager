import {
  Avatar,
  Button,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import { useState } from "react";
import { updatePhotoURL } from "../../../services/users.services";
import { useAuth } from "../../../services/auth.services";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { profilePicturesRef } from "../../../config/firebase-config";

const ProfilePicture = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [photo, setPhoto] = useState("");

  const { currentUser } = useAuth();
  const photoURL = currentUser?.photoURL ? currentUser.photoURL : null;

  const handlePhoto = (e) => {
    if (e.target.files[0]) {
      setPhoto(e.target.files[0]);
    }
  };

  const getImageURL = async () => {
    if (photo) {
      try {
        const uniqueImageName = currentUser.uid;
        const imageRef = ref(profilePicturesRef, uniqueImageName);
        await uploadBytes(imageRef, photo);
        const url = await getDownloadURL(imageRef);

        return url;
      } catch (error) {
        console.log(error);
      }
    }
    return null;
  };

  const uploadPicture = async (e) => {
    e.preventDefault();
    const url = await getImageURL();
    updatePhotoURL(currentUser.uid, url);
    setPhoto("");
    onClose();
  };

  return (
    <>
      <Avatar
        cursor="pointer"
        onClick={onOpen}
        name={currentUser.email}
        src={photoURL}
      />
      <Modal
        isCentered
        onClose={onClose}
        isOpen={isOpen}
        motionPreset="slideInBottom"
      >
        <ModalOverlay />
        <form onSubmit={uploadPicture}>
          <ModalContent>
            <ModalHeader>Change Profile Picture</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Input onChange={handlePhoto} type="file" />
            </ModalBody>
            <ModalFooter>
              <Button type="submit" colorScheme="teal" mr={3}>
                Upload
              </Button>
              <Button
                onClick={() => {
                  onClose();
                  setPhoto("");
                }}
              >
                Cancel
              </Button>
            </ModalFooter>
          </ModalContent>
        </form>
      </Modal>
    </>
  );
};
export default ProfilePicture;
