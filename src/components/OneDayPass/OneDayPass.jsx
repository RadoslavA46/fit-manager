import "./OneDayPass.css";
import NewMemberForm from "../MainPage/NewMemberForm";
import PayButton from "../PayButton/PayButton";
import SearchModal from "../MainPage/SearchModal/Searchmodal";
import { logTicket } from "../../services/log.services";
import { useContext } from "react";
import { BasketContext } from "../../contexts/BasketContext";

const OneDayPass = ({ info }) => {
  const { basket, updateBasket } = useContext(BasketContext)

  const payAndRegister = () => {
    //delete from Desktop and register in DB
    logTicket(info.member, info.date, info.price, info.id);
    const updatedBasket = basket.filter((item) => item.id !== info.id);
    updateBasket(updatedBasket);
    console.log('Deleted and registered')
  };

  const handleMemberSelect = (name) => {
    const updated = basket.map((item) => {
      if (item.id === info.id) {
        return { ...item, member: name };
      }
      return item;
    });
    updateBasket(updated); 
  };

  return (
    <>
      {
        <div className="ticket">
          <div className="ticket-header">Day Pass</div>

          {!info.member ? (
            <div>
              <div>
                {" "}
                <NewMemberForm handleMemberSelect={handleMemberSelect} />{" "}
              </div>
              <p>or</p>
              <div>
                {" "}
                <SearchModal handleMemberSelect={handleMemberSelect} />{" "}
              </div>
              <p>member</p>
            </div>
          ) : (
            <div>
              <p style={{ margin: "10px" }}>{info.member}</p>
              <p style={{ margin: "10px" }}>{info.date}</p>
              <p>
                <PayButton price={info.price} payAndRegister={payAndRegister} />
              </p>
            </div>
          )}
        </div>
      }
    </>
  );
};

export default OneDayPass;
