import "./Membership.css";
import NewMemberForm from "../MainPage/NewMemberForm";
import PayButton from "../PayButton/PayButton";
import SearchModal from "../MainPage/SearchModal/Searchmodal";
import DateRangePicker from "./DateRangePicker/DateRangePicker";
import { logMembership } from "../../services/log.services";
import { useContext } from "react";
import { BasketContext } from "../../contexts/BasketContext";

const Membership = ({ info }) => {
  const { basket, updateBasket } = useContext(BasketContext)

  const payAndRegister = () => {
    //delete and register in DB
    logMembership(
      info.member,
      info.date,
      info.price,
      info.id,
      info.from,
      info.to,
    );
    const updatedBasket = basket.filter((item) => item.id !== info.id);
    updateBasket(updatedBasket);
  };
 
  const handleMemberSelect = (name) => {
    const updated = basket.map((item) => {
      if (item.id === info.id) {
        return { ...item, member: name };
      }
      return item;
    });
    updateBasket(updated);
  };

  const handleDateSelect = (id, from, to) => {
    const updated = basket.map((item) => {
      if (item.id === id) {
        return { ...item, from, to };
      }
      return item;
    });
    updateBasket(updated);
  };

  const handlePriceSelect = (id, price) => {
    const updated = basket.map((item) => {
      if (item.id === id) {
        return { ...item, price };
      }
      return item;
    });
    updateBasket(updated);
  };

  return (
    <>
      {
        <div className="ticket">
          <div className="ticket-header">Membership</div>

          {!info.member ? (
            <div>
              <div>
                {" "}
                <NewMemberForm handleMemberSelect={handleMemberSelect} />{" "}
              </div>
              <p>or</p>
              <div>
                {" "}
                <SearchModal handleMemberSelect={handleMemberSelect} />{" "}
              </div>
              <p>member</p>
            </div>
          ) : (
            <div>
              <p>{info.member}</p>
              <DateRangePicker
                info={info}
                handleDateSelect={handleDateSelect}
                handlePriceSelect={handlePriceSelect}
              />
              {info.to && (
                <p>
                  <PayButton
                    price={info.price}
                    payAndRegister={payAndRegister}
                  />
                </p>
              )}
            </div>
          )}
        </div>
      }
    </>
  );
};

export default Membership;
