import { useEffect, useState } from "react";
import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Stack,
  Input,
  ModalFooter,
} from "@chakra-ui/react";
import dayjs from "dayjs";
import {
  ONE_MONTH,
  ONE_YEAR,
  SIX_MONTHS,
  THREE_MONTHS,
} from "../../../consts/prices";

const DateRangePicker = ({ handleDateSelect, info, handlePriceSelect }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const [selectedDuration, setSelectedDuration] = useState(null);

  useEffect(() => {
    if (info.from && info.to) {
      const fromDate = dayjs(info.from, "YYYY-MM-DD");
      const toDate = dayjs(info.to, "YYYY-MM-DD");
      const durationInMonths = toDate.diff(fromDate, "month");

      if (durationInMonths === 1) {
        setSelectedOption({ label: "1 Month", value: 1 });
      } else if (durationInMonths === 3) {
        setSelectedOption({ label: "3 Months", value: 3 });
      } else if (durationInMonths === 6) {
        setSelectedOption({ label: "6 Months", value: 6 });
      } else if (durationInMonths === 12) {
        setSelectedOption({ label: "1 Year", value: 12 });
      }
    }
  }, [info.from, info.to]);

  const handleOptionSelect = (option) => {
    handlePriceSelect(info.id, getPriceForOption(option.value));
    setSelectedOption(option);
    setSelectedDuration(option.value);
    setIsOpen(false);
  };

  const getPriceForOption = (value) => {
    switch (value) {
      case 1:
        return ONE_MONTH;
      case 3:
        return THREE_MONTHS;
      case 6:
        return SIX_MONTHS;
      case 12:
        return ONE_YEAR;
      default:
        return 0;
    }
  };

  const handleFromDateChange = (event) => {
    const newFromDate = dayjs(event.target.value, "YYYY-MM-DD");
    const newToDate = newFromDate.add(selectedDuration, "month");
    handleDateSelect(
      info.id,
      newFromDate.format("YYYY-MM-DD"),
      newToDate.format("YYYY-MM-DD"),
    );
  };

  const options = [
    { label: "1 Month", value: 1 },
    { label: "3 Months", value: 3 },
    { label: "6 Months", value: 6 },
    { label: "1 Year", value: 12 },
  ];

  const formattedToDate = info.to
    ? `${info.to.substr(8, 2)}/${info.to.substr(5, 2)}/${info.to.substr(0, 4)}`
    : null;

  return (
    <div>
      {selectedOption ? (
        <>
          <p style={{ fontWeight: "bold" }}>{selectedOption.label}</p>
          <div>
            From:{" "}
            <Input
              type="date"
              value={info.from || ''}
              onChange={handleFromDateChange}
            />
          </div>
          <div>To: {formattedToDate}</div>
        </>
      ) : (
        <div>
          <Button onClick={() => setIsOpen(true)}>Choose</Button>
          <p>Duration</p>
        </div>
      )}

      <Modal isOpen={isOpen} onClose={() => setIsOpen(false)}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Select a Duration</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Stack spacing={2}>
              {options.map((option) => (
                <Button
                  key={option.value}
                  onClick={() => handleOptionSelect(option)}
                >
                  {option.label}
                </Button>
              ))}
            </Stack>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" onClick={() => setIsOpen(false)}>
              Cancel
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
};

export default DateRangePicker;
