import React, { useState } from "react";
import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  Tabs,
  TabList,
  Tab,
  TabPanel,
  TabPanels,
  VStack,
  Center,
  Box,
  Text,
  DatePicker,
} from "@chakra-ui/react";
import { addMonths, addYears, format } from "date-fns";

const DateRangePicker = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedTab, setSelectedTab] = useState("1M"); // Default tab is 1 month
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const handleTabChange = (tab) => {
    setSelectedTab(tab);

    const currentDate = new Date();
    if (tab === "1M") {
      setEndDate(addMonths(currentDate, 1));
    } else if (tab === "3M") {
      setEndDate(addMonths(currentDate, 3));
    } else if (tab === "6M") {
      setEndDate(addMonths(currentDate, 6));
    } else if (tab === "1Y") {
      setEndDate(addYears(currentDate, 1));
    }
  };

  const handleStartDateChange = (date) => {
    setStartDate(date);
  };

  const handleModalOpen = () => {
    setIsOpen(true);
  };

  const handleModalClose = () => {
    setIsOpen(false);
  };

  const handleConfirm = () => {
    console.log("Start Date:", format(startDate, "yyyy-MM-dd"));
    console.log("End Date:", format(endDate, "yyyy-MM-dd"));
    handleModalClose();
  };

  return (
    <>
      <Button onClick={handleModalOpen}>Select Date Range</Button>
      <Modal isOpen={isOpen} onClose={handleModalClose} size="lg">
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Select Date Range</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Tabs isFitted variant="enclosed" onChange={handleTabChange}>
              <TabList>
                <Tab _selected={{ color: "white", bg: "blue.500" }} value="1M">
                  1 Month
                </Tab>
                <Tab _selected={{ color: "white", bg: "blue.500" }} value="3M">
                  3 Months
                </Tab>
                <Tab _selected={{ color: "white", bg: "blue.500" }} value="6M">
                  6 Months
                </Tab>
                <Tab _selected={{ color: "white", bg: "blue.500" }} value="1Y">
                  1 Year
                </Tab>
              </TabList>
              <TabPanels>
                <TabPanel>
                  <VStack spacing={4} align="start">
                    <Box>
                      <Text>Start Date:</Text>
                      <DatePicker
                        selected={startDate}
                        onChange={handleStartDateChange}
                      />
                    </Box>
                  </VStack>
                </TabPanel>
              </TabPanels>
            </Tabs>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" onClick={handleConfirm}>
              Confirm
            </Button>
            <Button onClick={handleModalClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default DateRangePicker;
