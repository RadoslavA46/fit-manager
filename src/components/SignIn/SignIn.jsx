import { useState } from "react";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Input,
  Heading,
  Stack,
  Text,
  Link,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { signIn, useAuth } from "../../services/auth.services";

const SignIn = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { currentUser } = useAuth();

  const navigate = useNavigate();

  const handleSignIn = async (e) => {
    e.preventDefault();
    try {
      await signIn(email, password);
      navigate("/");
    } catch (error) {
      console.error(error);
      alert("Log in was not successful.");
      navigate("/sign-up");
    }
  };

  return (
    <Box maxW="md" mx="auto" mt={8} p={4}>
      <Heading size="lg" textAlign="center" mb={6}>
        Sign In
      </Heading>
      {Boolean(currentUser) && currentUser.email}
      <form onSubmit={handleSignIn}>
        <Stack spacing={4}>
          <FormControl>
            <FormLabel>Email</FormLabel>
            <Input
              type="email"
              placeholder="Enter your email"
              autoComplete="username"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </FormControl>
          <FormControl>
            <FormLabel>Password</FormLabel>
            <Input
              type="password"
              placeholder="Enter your password"
              autoComplete="current-password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormControl>
          <Button colorScheme="blue" type="submit">
            Sign In
          </Button>
        </Stack>
      </form>
      <Text marginTop='5px' textAlign="center">
        Don't have an account?
        <Link onClick={() => navigate("/sign-up")} color="blue.500" href="#">
          Sign up HERE!
        </Link>
      </Text>
    </Box>
  );
};

export default SignIn;
