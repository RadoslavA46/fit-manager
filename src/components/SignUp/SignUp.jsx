import { useState } from "react";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Input,
  Heading,
  Stack,
  Text,
  Link,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { registerNewUser, useAuth } from "../../services/auth.services";

const MIN_PASSWORD_LENGTH = 7;

const SignUp = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const navigate = useNavigate();
  const { currentUser } = useAuth();

  const handleSignUp = async (e) => {
    e.preventDefault();
    try {
      if (password.length < MIN_PASSWORD_LENGTH) {
        alert(`Password must be at least ${MIN_PASSWORD_LENGTH} characters`);
      } else if (password !== confirmPassword) {
        alert("Passwords don't match");
      } else {
        await registerNewUser(email, password);
        navigate("/");
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <Box maxW="md" mx="auto" mt={8} p={4}>
      {Boolean(currentUser) && currentUser.email}
      <Heading size="lg" textAlign="center" mb={6}>
        Sign Up
      </Heading>
      <form onSubmit={handleSignUp}>
        <Stack spacing={4}>
          <FormControl>
            <FormLabel>Email</FormLabel>
            <Input
              type="email"
              placeholder="Enter your email"
              value={email}
              autoComplete="username"
              onChange={(e) => setEmail(e.target.value)}
            />
          </FormControl>
          <FormControl>
            <FormLabel>Password</FormLabel>
            <Input
              type="password"
              placeholder="Enter your password"
              value={password}
              autoComplete="new-password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormControl>
          <FormControl>
            <FormLabel>Confirm Password</FormLabel>
            <Input
              type="password"
              placeholder="Confirm your password"
              value={confirmPassword}
              autoComplete="new-password"
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </FormControl>
          <Button colorScheme="blue" type="submit">
            Sign Up
          </Button>
          <Text textAlign="center">
            Already have an account?{" "}
            <Link color="blue.500" onClick={() => navigate("/sign-in")}>
              Log in HERE!
            </Link>
          </Text>
        </Stack>
      </form>
    </Box>
  );
};

export default SignUp;
