import { useAuth } from "../services/auth.services";
import { Navigate, Outlet } from 'react-router-dom'

const PrivateRoutes = () => {
  const { currentUser } = useAuth();
return (
    currentUser ? <Outlet/> : <Navigate to="/sign-in"/>
  )
}
export default PrivateRoutes;

