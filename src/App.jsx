import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import MainPage from "./components/MainPage/MainPage/MainPage.jsx";
import SignIn from "./components/SignIn/SignIn";
import SignUp from "./components/SignUp/SignUp";
import { BasketContextProvider } from "./contexts/BasketContext";
import LogView from "./components/MainPage/LogView/LogView.jsx";
import ProductsView from "./components/MainPage/ProductsOverview/ProductsView.jsx";
import AdminPanelView from "./components/MainPage/AdminPanel/AdminPanelView.jsx";
import PrivateRoutes from "./components/PrivateRoutes.jsx";




function App() {



  return (
    <>
      <BasketContextProvider>
        <Router>
          <Routes>
            <Route element={<PrivateRoutes/>}>
              <Route path="/" element={<MainPage/>} />
              <Route path="/products" element={<ProductsView/>} />
              <Route path="/admin-panel" element={<AdminPanelView/>} />
              <Route path="/log" element={<LogView/>} />
            </Route>
            <Route path="/sign-in" element={<SignIn/>} />
            <Route path="/sign-up" element={<SignUp/>} />

          </Routes>
        </Router>
      </BasketContextProvider>
    </>
  );
}

export default App;
