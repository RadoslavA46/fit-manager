import { v4 as uuidv4 } from "uuid";

export const keyGenerator = () => {
  return uuidv4();
};
