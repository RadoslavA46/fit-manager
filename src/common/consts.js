export const NAME_MAX_LENGTH = 20;
export const NAME_MIN_LENGTH = 2;
export const PASS_MAX_LENGTH = 50;
export const PASS_MIN_LENGTH = 8;
export const MIN_ADDRESS_LENGTH = 2;
export const MAX_ADDRESS_LENGTH = 50;
export const CPR_LENGTH = 10;