import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";
import { getAuth } from "firebase/auth";
import { getStorage, ref } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCwaWAmPlrC4PVMnV41S1dGb5mFkuqVFvw",
  authDomain: "fit-manager-b8145.firebaseapp.com",
  databaseURL:
    "https://fit-manager-b8145-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "fit-manager-b8145",
  storageBucket: "fit-manager-b8145.appspot.com",
  messagingSenderId: "531359464122",
  appId: "1:531359464122:web:c5a1732fed014645542083",
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
export const storage = getStorage(app);
export const storageRef = ref(storage);
export const profilePicturesRef = ref(storage, "/profilePictures");
export const productPicturesRef = ref(storage, "/productPictures");
