export const ONE_DAY = 10;
export const ONE_MONTH = 100;
export const THREE_MONTHS = 300;
export const SIX_MONTHS = 600;
export const ONE_YEAR = 1200;
