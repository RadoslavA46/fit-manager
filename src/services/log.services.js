import { ref, set } from "firebase/database";
import { db } from "../config/firebase-config";

export const logTicket = (member, date, price, id) => {
  try {
    set(ref(db, `log/tickets/${id}`), {
      member,
      date,
      price,
      id,
    });
  } catch (error) {
    console.log(error.message);
  }
};

export const logMembership = (member, date, price, id, from, to) => {
  try {
    set(ref(db, `log/memberships/${id}`), {
      member,
      date,
      price,
      from,
      to,
      id,
    });
  } catch (error) {
    console.log(error);
  }
};
