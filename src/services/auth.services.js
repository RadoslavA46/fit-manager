import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import { auth } from "../config/firebase-config";
import { createUserDB } from "./users.services";
import { createContext, useContext } from "react";

export const AuthContext = createContext();

export const useAuth = () => {
  return useContext(AuthContext);
};

export const registerNewUser = async (email, password) => {
  //return auth.createUserWithEmailAndPassword(email, password)
  const userCredential = await createUserWithEmailAndPassword(
    auth,
    email,
    password,
  );
  return createUserDB(userCredential.user.uid, email);
};

export const signIn = (email, password) => {
  try {
    return signInWithEmailAndPassword(auth, email, password);
  } catch (error) {
    console.log(error.message);
  }
};

export const logOutUser = () => {
  return signOut(auth);
};
