import { get, ref, set } from "firebase/database";
import { db } from "../config/firebase-config.js";

export const createUserDB = (uid, email) => {
  try {
    return set(ref(db, `users/${uid}`), { email, admin: false, uid });
  } catch (error) {
    console.log(error);
  }
};

export const getUserByUid = async (uid) => {
  try {
    const snapshot = await get(ref(db, `users/${uid}`));
    if (snapshot.exists()) {
      const userData = snapshot.val();
      return userData;
    }
  } catch (error) {
    console.error(error);
  }
};

export const updatePhotoURL = (uid, url) => {
  set(ref(db, `users/${uid}/photoURL/`), url);
};

export const toggleAdminUser = async (uid) => {
  const isAdminRef = ref(db, `users/${uid}/admin`);

  try {
    const isAdminSnapshot = await get(isAdminRef);
    const currentIsAdminValue = isAdminSnapshot.val();

    await set(isAdminRef, !currentIsAdminValue);

    console.log(
      `Member with uid ${uid} is now ${
        !currentIsAdminValue ? "admin" : "user"
      }.`,
    );
  } catch (error) {
    console.error("Error toggling admin status:", error);
  }
};
