import { get, ref, remove, set } from "firebase/database";
import { db } from "../config/firebase-config";

export const registerMember = (firstName, lastName, sex, address, cprNr) => {
  set(ref(db, "members/" + cprNr), {
    firstName,
    lastName,
    sex,
    address,
    cprNr,
    registered: `${new Date()}`,
  });
};

export const toggleBanMember = async (cprNr) => {
  // Reference to the 'isBanned' field of the member with the given CPR number
  const isBannedRef = ref(db, `members/${cprNr}/isBanned`);

  try {
    // Get the current value of 'isBanned'
    const isBannedSnapshot = await get(isBannedRef);
    const currentIsBannedValue = isBannedSnapshot.val();

    // Toggle the value and set it back in the database
    await set(isBannedRef, !currentIsBannedValue);

    console.log(
      `Member with CPR ${cprNr} is now ${
        !currentIsBannedValue ? "banned" : "allowed"
      }.`,
    );
  } catch (error) {
    console.error("Error toggling ban status:", error);
  }
};

export const deleteMember = (cprNr) => {
  remove(ref(db, `members/${cprNr}`));
};
