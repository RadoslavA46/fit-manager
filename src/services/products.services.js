import { get, ref, remove, set } from "firebase/database";
import { db } from "../config/firebase-config";
import { getDate, getTime } from "./date.and.time.services";
import { keyGenerator } from "../common/key.generator";


export const addNewProduct = (
  productName,
  productPrice,
  imageURL,
  productID,
) => {
  set(ref(db, "products/" + productID), {
    productName,
    price: productPrice,
    imageURL,
    id: productID,
    type: 'product'
  });
};

export const registerPaidProducts = (basketArray) => {
  
  basketArray.forEach((basketItem) => {
    const id = keyGenerator()
    if (basketItem.type === 'product') {
      set(ref(db, "log/" + "products/" + id), {
        info: `${basketItem.count} x ${basketItem.price}$ ${basketItem.productName
          } ${basketItem.count * basketItem.price}$`,
        date: `${getDate()}, ${getTime()}`,
        price: basketItem.count * basketItem.price,
        id: id
      });
    }
  });
};

export const loadProducts = async () => {
  try {
    const snapshot = await get(ref(db, "products"));

    if (snapshot.exists()) {
      const products = [];

      snapshot.forEach((childSnapshot) => {
        const productData = childSnapshot.val();
        products.push(productData);
      });
      return products;
    } else {
      console.log("No data available");
      return [];
    }
  } catch (error) {
    console.error("Error loading products:", error);
    return [];
  }
};

export const deleteProduct = (id) => {
  remove(ref(db, `/products/${id}`));
};
