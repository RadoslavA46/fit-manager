export const getDateAndTime = () => {
  const currentDate = new Date();
  const day = String(currentDate.getDate()).padStart(2, "0");
  const month = String(currentDate.getMonth() + 1).padStart(2, "0"); // Month is zero-based
  const year = currentDate.getFullYear();
  const formattedDate = `${day}-${month}-${year}`;
  const time = currentDate.toLocaleTimeString("en-US", { hour12: false });
  return [formattedDate, time];
};

export const getDate = () => {
  const currentDate = new Date();
  const day = String(currentDate.getDate()).padStart(2, "0");
  const month = String(currentDate.getMonth() + 1).padStart(2, "0"); // Month is zero-based
  const year = currentDate.getFullYear();
  const formattedDate = `${day}-${month}-${year}`;

  return formattedDate;
};

export const getTime = () => {
  const currentDate = new Date();
  const time = currentDate.toLocaleTimeString("en-US", { hour12: false });
  return time;
};
