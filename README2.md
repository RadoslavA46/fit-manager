# Fit Manager

Fit Manager is a B2B program designed for fitness owners to efficiently manage their gym members and streamline the handling of memberships, day passes, and various products available for sale within the gym, such as drinks, supplements, and more.

## Getting started

Open the /src folder in the terminal and run the following commands:

- **npm install**
- **npm run dev**

## User Access

- **Regular User Access:**

  - Create a new user with limited access.
  - Or log in with the provided credentials:
    - Username: user@fitmanager.com
    - Password: 1234567

- **Admin Access:**
  - Username: admin@fitmanager.com
  - Password: 1234567

## Functionality

### Passes and Memberships

- Sell day passes or memberships (1, 3, 6, or 12 months).
- Create and delete gym members.
- Assign members to passes or memberships.
- Automatically calculate the 'to' date when selecting the 'from' date.

### Admin Panel

- Accessible only for admin users.
- Add new products for sale with names, prices, and photos.
- Ban or delete existing members.
- Grant admin rights to regular users.

### Log

- View sales entries with detailed information.
- Calculate total sales.
- Delete entries if a mistake is identified.

### Products Functionality

- Render cards for products with images.
- Adjust the quantity of products using + and - buttons.
- Admins can delete existing products or add new ones.

## Tech Stack

- JavaScript
- React
- Chakra UI
- Google Firebase
- GitLab
- Node.js

Fit Manager provides a comprehensive solution for fitness business owners, ensuring efficient management of members and sales while offering a user-friendly interface.
